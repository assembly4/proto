// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.18.0
// source: zug/zug.proto

package zug

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Status struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status string `protobuf:"bytes,1,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *Status) Reset() {
	*x = Status{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zug_zug_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Status) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Status) ProtoMessage() {}

func (x *Status) ProtoReflect() protoreflect.Message {
	mi := &file_zug_zug_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Status.ProtoReflect.Descriptor instead.
func (*Status) Descriptor() ([]byte, []int) {
	return file_zug_zug_proto_rawDescGZIP(), []int{0}
}

func (x *Status) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

type Options struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	DeviceId string `protobuf:"bytes,1,opt,name=device_id,json=deviceId,proto3" json:"device_id,omitempty"`
	Token    string `protobuf:"bytes,2,opt,name=token,proto3" json:"token,omitempty"`
}

func (x *Options) Reset() {
	*x = Options{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zug_zug_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Options) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Options) ProtoMessage() {}

func (x *Options) ProtoReflect() protoreflect.Message {
	mi := &file_zug_zug_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Options.ProtoReflect.Descriptor instead.
func (*Options) Descriptor() ([]byte, []int) {
	return file_zug_zug_proto_rawDescGZIP(), []int{1}
}

func (x *Options) GetDeviceId() string {
	if x != nil {
		return x.DeviceId
	}
	return ""
}

func (x *Options) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

type Devices struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Uuids []string `protobuf:"bytes,1,rep,name=uuids,proto3" json:"uuids,omitempty"`
	Token string   `protobuf:"bytes,2,opt,name=token,proto3" json:"token,omitempty"`
}

func (x *Devices) Reset() {
	*x = Devices{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zug_zug_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Devices) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Devices) ProtoMessage() {}

func (x *Devices) ProtoReflect() protoreflect.Message {
	mi := &file_zug_zug_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Devices.ProtoReflect.Descriptor instead.
func (*Devices) Descriptor() ([]byte, []int) {
	return file_zug_zug_proto_rawDescGZIP(), []int{2}
}

func (x *Devices) GetUuids() []string {
	if x != nil {
		return x.Uuids
	}
	return nil
}

func (x *Devices) GetToken() string {
	if x != nil {
		return x.Token
	}
	return ""
}

type LocationWithSetup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Types that are assignable to Content:
	//	*LocationWithSetup_Options
	//	*LocationWithSetup_Location
	Content isLocationWithSetup_Content `protobuf_oneof:"content"`
}

func (x *LocationWithSetup) Reset() {
	*x = LocationWithSetup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zug_zug_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *LocationWithSetup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*LocationWithSetup) ProtoMessage() {}

func (x *LocationWithSetup) ProtoReflect() protoreflect.Message {
	mi := &file_zug_zug_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use LocationWithSetup.ProtoReflect.Descriptor instead.
func (*LocationWithSetup) Descriptor() ([]byte, []int) {
	return file_zug_zug_proto_rawDescGZIP(), []int{3}
}

func (m *LocationWithSetup) GetContent() isLocationWithSetup_Content {
	if m != nil {
		return m.Content
	}
	return nil
}

func (x *LocationWithSetup) GetOptions() *Options {
	if x, ok := x.GetContent().(*LocationWithSetup_Options); ok {
		return x.Options
	}
	return nil
}

func (x *LocationWithSetup) GetLocation() *Location {
	if x, ok := x.GetContent().(*LocationWithSetup_Location); ok {
		return x.Location
	}
	return nil
}

type isLocationWithSetup_Content interface {
	isLocationWithSetup_Content()
}

type LocationWithSetup_Options struct {
	Options *Options `protobuf:"bytes,1,opt,name=options,proto3,oneof"`
}

type LocationWithSetup_Location struct {
	Location *Location `protobuf:"bytes,2,opt,name=location,proto3,oneof"`
}

func (*LocationWithSetup_Options) isLocationWithSetup_Content() {}

func (*LocationWithSetup_Location) isLocationWithSetup_Content() {}

type Location struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	DeviceId string  `protobuf:"bytes,1,opt,name=device_id,json=deviceId,proto3" json:"device_id,omitempty"`
	Time     string  `protobuf:"bytes,3,opt,name=time,proto3" json:"time,omitempty"`
	Lat      float32 `protobuf:"fixed32,4,opt,name=lat,proto3" json:"lat,omitempty"`
	Long     float32 `protobuf:"fixed32,5,opt,name=long,proto3" json:"long,omitempty"`
}

func (x *Location) Reset() {
	*x = Location{}
	if protoimpl.UnsafeEnabled {
		mi := &file_zug_zug_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Location) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Location) ProtoMessage() {}

func (x *Location) ProtoReflect() protoreflect.Message {
	mi := &file_zug_zug_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Location.ProtoReflect.Descriptor instead.
func (*Location) Descriptor() ([]byte, []int) {
	return file_zug_zug_proto_rawDescGZIP(), []int{4}
}

func (x *Location) GetDeviceId() string {
	if x != nil {
		return x.DeviceId
	}
	return ""
}

func (x *Location) GetTime() string {
	if x != nil {
		return x.Time
	}
	return ""
}

func (x *Location) GetLat() float32 {
	if x != nil {
		return x.Lat
	}
	return 0
}

func (x *Location) GetLong() float32 {
	if x != nil {
		return x.Long
	}
	return 0
}

var File_zug_zug_proto protoreflect.FileDescriptor

var file_zug_zug_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x7a, 0x75, 0x67, 0x2f, 0x7a, 0x75, 0x67, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x03, 0x7a, 0x75, 0x67, 0x22, 0x20, 0x0a, 0x06, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x16,
	0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x22, 0x3c, 0x0a, 0x07, 0x4f, 0x70, 0x74, 0x69, 0x6f, 0x6e,
	0x73, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x64, 0x12, 0x14,
	0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74,
	0x6f, 0x6b, 0x65, 0x6e, 0x22, 0x35, 0x0a, 0x07, 0x44, 0x65, 0x76, 0x69, 0x63, 0x65, 0x73, 0x12,
	0x14, 0x0a, 0x05, 0x75, 0x75, 0x69, 0x64, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x09, 0x52, 0x05,
	0x75, 0x75, 0x69, 0x64, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x22, 0x75, 0x0a, 0x11, 0x4c,
	0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x57, 0x69, 0x74, 0x68, 0x53, 0x65, 0x74, 0x75, 0x70,
	0x12, 0x28, 0x0a, 0x07, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x0c, 0x2e, 0x7a, 0x75, 0x67, 0x2e, 0x4f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x48,
	0x00, 0x52, 0x07, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x2b, 0x0a, 0x08, 0x6c, 0x6f,
	0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x7a,
	0x75, 0x67, 0x2e, 0x4c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x48, 0x00, 0x52, 0x08, 0x6c,
	0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x42, 0x09, 0x0a, 0x07, 0x63, 0x6f, 0x6e, 0x74, 0x65,
	0x6e, 0x74, 0x22, 0x61, 0x0a, 0x08, 0x4c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1b,
	0x0a, 0x09, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x49, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x74,
	0x69, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x69, 0x6d, 0x65, 0x12,
	0x10, 0x0a, 0x03, 0x6c, 0x61, 0x74, 0x18, 0x04, 0x20, 0x01, 0x28, 0x02, 0x52, 0x03, 0x6c, 0x61,
	0x74, 0x12, 0x12, 0x0a, 0x04, 0x6c, 0x6f, 0x6e, 0x67, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52,
	0x04, 0x6c, 0x6f, 0x6e, 0x67, 0x32, 0xae, 0x01, 0x0a, 0x05, 0x52, 0x65, 0x64, 0x69, 0x73, 0x12,
	0x37, 0x0a, 0x12, 0x53, 0x75, 0x62, 0x73, 0x63, 0x72, 0x69, 0x62, 0x65, 0x54, 0x6f, 0x44, 0x65,
	0x76, 0x69, 0x63, 0x65, 0x73, 0x12, 0x0c, 0x2e, 0x7a, 0x75, 0x67, 0x2e, 0x44, 0x65, 0x76, 0x69,
	0x63, 0x65, 0x73, 0x1a, 0x0d, 0x2e, 0x7a, 0x75, 0x67, 0x2e, 0x4c, 0x6f, 0x63, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x22, 0x00, 0x28, 0x01, 0x30, 0x01, 0x12, 0x3a, 0x0a, 0x0f, 0x53, 0x74, 0x72, 0x65,
	0x61, 0x6d, 0x44, 0x65, 0x76, 0x69, 0x63, 0x65, 0x4c, 0x6f, 0x63, 0x12, 0x16, 0x2e, 0x7a, 0x75,
	0x67, 0x2e, 0x4c, 0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x57, 0x69, 0x74, 0x68, 0x53, 0x65,
	0x74, 0x75, 0x70, 0x1a, 0x0b, 0x2e, 0x7a, 0x75, 0x67, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x22, 0x00, 0x28, 0x01, 0x12, 0x30, 0x0a, 0x10, 0x50, 0x75, 0x62, 0x6c, 0x69, 0x73, 0x68, 0x44,
	0x65, 0x76, 0x69, 0x63, 0x65, 0x4c, 0x6f, 0x63, 0x12, 0x0d, 0x2e, 0x7a, 0x75, 0x67, 0x2e, 0x4c,
	0x6f, 0x63, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x0b, 0x2e, 0x7a, 0x75, 0x67, 0x2e, 0x53, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0x00, 0x42, 0x24, 0x5a, 0x22, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62,
	0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x73, 0x73, 0x65, 0x6d, 0x62, 0x6c, 0x79, 0x34, 0x2f, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x7a, 0x75, 0x67, 0x3b, 0x7a, 0x75, 0x67, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_zug_zug_proto_rawDescOnce sync.Once
	file_zug_zug_proto_rawDescData = file_zug_zug_proto_rawDesc
)

func file_zug_zug_proto_rawDescGZIP() []byte {
	file_zug_zug_proto_rawDescOnce.Do(func() {
		file_zug_zug_proto_rawDescData = protoimpl.X.CompressGZIP(file_zug_zug_proto_rawDescData)
	})
	return file_zug_zug_proto_rawDescData
}

var file_zug_zug_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_zug_zug_proto_goTypes = []interface{}{
	(*Status)(nil),            // 0: zug.Status
	(*Options)(nil),           // 1: zug.Options
	(*Devices)(nil),           // 2: zug.Devices
	(*LocationWithSetup)(nil), // 3: zug.LocationWithSetup
	(*Location)(nil),          // 4: zug.Location
}
var file_zug_zug_proto_depIdxs = []int32{
	1, // 0: zug.LocationWithSetup.options:type_name -> zug.Options
	4, // 1: zug.LocationWithSetup.location:type_name -> zug.Location
	2, // 2: zug.Redis.SubscribeToDevices:input_type -> zug.Devices
	3, // 3: zug.Redis.StreamDeviceLoc:input_type -> zug.LocationWithSetup
	4, // 4: zug.Redis.PublishDeviceLoc:input_type -> zug.Location
	4, // 5: zug.Redis.SubscribeToDevices:output_type -> zug.Location
	0, // 6: zug.Redis.StreamDeviceLoc:output_type -> zug.Status
	0, // 7: zug.Redis.PublishDeviceLoc:output_type -> zug.Status
	5, // [5:8] is the sub-list for method output_type
	2, // [2:5] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_zug_zug_proto_init() }
func file_zug_zug_proto_init() {
	if File_zug_zug_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_zug_zug_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Status); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zug_zug_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Options); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zug_zug_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Devices); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zug_zug_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*LocationWithSetup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_zug_zug_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Location); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_zug_zug_proto_msgTypes[3].OneofWrappers = []interface{}{
		(*LocationWithSetup_Options)(nil),
		(*LocationWithSetup_Location)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_zug_zug_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_zug_zug_proto_goTypes,
		DependencyIndexes: file_zug_zug_proto_depIdxs,
		MessageInfos:      file_zug_zug_proto_msgTypes,
	}.Build()
	File_zug_zug_proto = out.File
	file_zug_zug_proto_rawDesc = nil
	file_zug_zug_proto_goTypes = nil
	file_zug_zug_proto_depIdxs = nil
}
