///
//  Generated code. Do not modify.
//  source: lucerne/lucerne.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'lucerne.pb.dart' as $0;
export 'lucerne.pb.dart';

class LocationClient extends $grpc.Client {
  static final _$insertLoc = $grpc.ClientMethod<$0.DeviceLocs, $0.Status>(
      '/lucerne.Location/InsertLoc',
      ($0.DeviceLocs value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Status.fromBuffer(value));
  static final _$selectLoc =
      $grpc.ClientMethod<$0.DeviceTimeRange, $0.DeviceLocs>(
          '/lucerne.Location/SelectLoc',
          ($0.DeviceTimeRange value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.DeviceLocs.fromBuffer(value));

  LocationClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.Status> insertLoc($0.DeviceLocs request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$insertLoc, request, options: options);
  }

  $grpc.ResponseStream<$0.DeviceLocs> selectLoc($0.DeviceTimeRange request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$selectLoc, $async.Stream.fromIterable([request]),
        options: options);
  }
}

abstract class LocationServiceBase extends $grpc.Service {
  $core.String get $name => 'lucerne.Location';

  LocationServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.DeviceLocs, $0.Status>(
        'InsertLoc',
        insertLoc_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeviceLocs.fromBuffer(value),
        ($0.Status value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeviceTimeRange, $0.DeviceLocs>(
        'SelectLoc',
        selectLoc_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.DeviceTimeRange.fromBuffer(value),
        ($0.DeviceLocs value) => value.writeToBuffer()));
  }

  $async.Future<$0.Status> insertLoc_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DeviceLocs> request) async {
    return insertLoc(call, await request);
  }

  $async.Stream<$0.DeviceLocs> selectLoc_Pre($grpc.ServiceCall call,
      $async.Future<$0.DeviceTimeRange> request) async* {
    yield* selectLoc(call, await request);
  }

  $async.Future<$0.Status> insertLoc(
      $grpc.ServiceCall call, $0.DeviceLocs request);
  $async.Stream<$0.DeviceLocs> selectLoc(
      $grpc.ServiceCall call, $0.DeviceTimeRange request);
}
