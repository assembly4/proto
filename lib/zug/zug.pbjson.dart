///
//  Generated code. Do not modify.
//  source: zug/zug.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use statusDescriptor instead')
const Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 9, '10': 'status'},
  ],
};

/// Descriptor for `Status`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List statusDescriptor = $convert.base64Decode('CgZTdGF0dXMSFgoGc3RhdHVzGAEgASgJUgZzdGF0dXM=');
@$core.Deprecated('Use optionsDescriptor instead')
const Options$json = const {
  '1': 'Options',
  '2': const [
    const {'1': 'device_id', '3': 1, '4': 1, '5': 9, '10': 'deviceId'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `Options`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List optionsDescriptor = $convert.base64Decode('CgdPcHRpb25zEhsKCWRldmljZV9pZBgBIAEoCVIIZGV2aWNlSWQSFAoFdG9rZW4YAiABKAlSBXRva2Vu');
@$core.Deprecated('Use devicesDescriptor instead')
const Devices$json = const {
  '1': 'Devices',
  '2': const [
    const {'1': 'uuids', '3': 1, '4': 3, '5': 9, '10': 'uuids'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `Devices`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List devicesDescriptor = $convert.base64Decode('CgdEZXZpY2VzEhQKBXV1aWRzGAEgAygJUgV1dWlkcxIUCgV0b2tlbhgCIAEoCVIFdG9rZW4=');
@$core.Deprecated('Use locationWithSetupDescriptor instead')
const LocationWithSetup$json = const {
  '1': 'LocationWithSetup',
  '2': const [
    const {'1': 'options', '3': 1, '4': 1, '5': 11, '6': '.zug.Options', '9': 0, '10': 'options'},
    const {'1': 'location', '3': 2, '4': 1, '5': 11, '6': '.zug.Location', '9': 0, '10': 'location'},
  ],
  '8': const [
    const {'1': 'content'},
  ],
};

/// Descriptor for `LocationWithSetup`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationWithSetupDescriptor = $convert.base64Decode('ChFMb2NhdGlvbldpdGhTZXR1cBIoCgdvcHRpb25zGAEgASgLMgwuenVnLk9wdGlvbnNIAFIHb3B0aW9ucxIrCghsb2NhdGlvbhgCIAEoCzINLnp1Zy5Mb2NhdGlvbkgAUghsb2NhdGlvbkIJCgdjb250ZW50');
@$core.Deprecated('Use locationDescriptor instead')
const Location$json = const {
  '1': 'Location',
  '2': const [
    const {'1': 'device_id', '3': 1, '4': 1, '5': 9, '10': 'deviceId'},
    const {'1': 'time', '3': 3, '4': 1, '5': 9, '10': 'time'},
    const {'1': 'lat', '3': 4, '4': 1, '5': 2, '10': 'lat'},
    const {'1': 'long', '3': 5, '4': 1, '5': 2, '10': 'long'},
  ],
};

/// Descriptor for `Location`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List locationDescriptor = $convert.base64Decode('CghMb2NhdGlvbhIbCglkZXZpY2VfaWQYASABKAlSCGRldmljZUlkEhIKBHRpbWUYAyABKAlSBHRpbWUSEAoDbGF0GAQgASgCUgNsYXQSEgoEbG9uZxgFIAEoAlIEbG9uZw==');
