///
//  Generated code. Do not modify.
//  source: zug/zug.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:async' as $async;

import 'package:protobuf/protobuf.dart' as $pb;

import 'dart:core' as $core;
import 'zug.pb.dart' as $0;
import 'zug.pbjson.dart';

export 'zug.pb.dart';

abstract class RedisServiceBase extends $pb.GeneratedService {
  $async.Future<$0.Location> subscribeToDevices($pb.ServerContext ctx, $0.Devices request);
  $async.Future<$0.Status> streamDeviceLoc($pb.ServerContext ctx, $0.LocationWithSetup request);
  $async.Future<$0.Status> publishDeviceLoc($pb.ServerContext ctx, $0.Location request);

  $pb.GeneratedMessage createRequest($core.String method) {
    switch (method) {
      case 'SubscribeToDevices': return $0.Devices();
      case 'StreamDeviceLoc': return $0.LocationWithSetup();
      case 'PublishDeviceLoc': return $0.Location();
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String method, $pb.GeneratedMessage request) {
    switch (method) {
      case 'SubscribeToDevices': return this.subscribeToDevices(ctx, request as $0.Devices);
      case 'StreamDeviceLoc': return this.streamDeviceLoc(ctx, request as $0.LocationWithSetup);
      case 'PublishDeviceLoc': return this.publishDeviceLoc(ctx, request as $0.Location);
      default: throw $core.ArgumentError('Unknown method: $method');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => RedisServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => RedisServiceBase$messageJson;
}

