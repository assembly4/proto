///
//  Generated code. Do not modify.
//  source: zug/zug.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'zug.pb.dart' as $0;
export 'zug.pb.dart';

class RedisClient extends $grpc.Client {
  static final _$subscribeToDevices =
      $grpc.ClientMethod<$0.Devices, $0.Location>(
          '/zug.Redis/SubscribeToDevices',
          ($0.Devices value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Location.fromBuffer(value));
  static final _$streamDeviceLoc =
      $grpc.ClientMethod<$0.LocationWithSetup, $0.Status>(
          '/zug.Redis/StreamDeviceLoc',
          ($0.LocationWithSetup value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Status.fromBuffer(value));
  static final _$publishDeviceLoc = $grpc.ClientMethod<$0.Location, $0.Status>(
      '/zug.Redis/PublishDeviceLoc',
      ($0.Location value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Status.fromBuffer(value));

  RedisClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseStream<$0.Location> subscribeToDevices(
      $async.Stream<$0.Devices> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$subscribeToDevices, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.Status> streamDeviceLoc(
      $async.Stream<$0.LocationWithSetup> request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$streamDeviceLoc, request, options: options)
        .single;
  }

  $grpc.ResponseFuture<$0.Status> publishDeviceLoc($0.Location request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$publishDeviceLoc, request, options: options);
  }
}

abstract class RedisServiceBase extends $grpc.Service {
  $core.String get $name => 'zug.Redis';

  RedisServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Devices, $0.Location>(
        'SubscribeToDevices',
        subscribeToDevices,
        true,
        true,
        ($core.List<$core.int> value) => $0.Devices.fromBuffer(value),
        ($0.Location value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.LocationWithSetup, $0.Status>(
        'StreamDeviceLoc',
        streamDeviceLoc,
        true,
        false,
        ($core.List<$core.int> value) => $0.LocationWithSetup.fromBuffer(value),
        ($0.Status value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Location, $0.Status>(
        'PublishDeviceLoc',
        publishDeviceLoc_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Location.fromBuffer(value),
        ($0.Status value) => value.writeToBuffer()));
  }

  $async.Future<$0.Status> publishDeviceLoc_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Location> request) async {
    return publishDeviceLoc(call, await request);
  }

  $async.Stream<$0.Location> subscribeToDevices(
      $grpc.ServiceCall call, $async.Stream<$0.Devices> request);
  $async.Future<$0.Status> streamDeviceLoc(
      $grpc.ServiceCall call, $async.Stream<$0.LocationWithSetup> request);
  $async.Future<$0.Status> publishDeviceLoc(
      $grpc.ServiceCall call, $0.Location request);
}
