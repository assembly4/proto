## Generate proto files for languages

Prepare environment for [go](https://grpc.io/docs/languages/go/quickstart/) and [dart](https://grpc.io/docs/languages/dart/quickstart/) according to documentation

Clone the project

```bash
  git clone git@gitlab.com:assembly4/proto.git
```

Go to the project directory

```bash
  cd proto
```

Generate proto files

```bash
for d in */ 
do 
  d=$(sed 's|/||g' <<< $d); 
  if [[ "$d" == "lib" ]]; then continue; fi; 
  mkdir -p ./lib/$d;
  protoc --dart_out=grpc:./lib/ --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative $d/$d.proto;
done
```

## Usage/Examples
First, tell go where to look for private packages
```bash
export GOPRIVATE=gitlab.com/assembly4;
```
In code
```go
import (
    "gitlab.com/assembly4/proto/zug"
    "gitlab.com/assembly4/proto/lucerne"
)
```
